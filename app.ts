import*as http from "http"
import*as path from 'path'
import*as fs from 'fs'
import { url } from "inspector"

const server = http.createServer((req,res) => {
    fs.readFile(path.join(__dirname,'/produce/index.html'), (err,content)=>{
        if (err) throw err
            if (req.url ==="/"){
               res.writeHead(200,{'Content-Type': 'text/html'})
               res.end(content)   
    } 
    })    
})
const PORT = process.env.PORT||8080;

server.listen(PORT,()=>console.log(`Server running on port: ${PORT}`))
