"use strict";
exports.__esModule = true;
var http = require("http");
var path = require("path");
var fs = require("fs");
var server = http.createServer(function (req, res) {
    fs.readFile(path.join(__dirname, '/produce/index.html'), function (err, content) {
        if (err)
            throw err;
        if (req.url === "/") {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end(content);
        }
    });
});
var PORT = process.env.PORT || 8080;
server.listen(PORT, function () { return console.log("Server running on port: ".concat(PORT)); });
