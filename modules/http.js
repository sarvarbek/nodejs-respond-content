"use strict";
exports.__esModule = true;
var http = require("http");
var server = http.createServer(function (req, res) {
    return res.end('Server running on port 3000');
}).listen(4000);
