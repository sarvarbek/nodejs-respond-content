"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var events_1 = require("events");
var events_enum_1 = require("./events-enum");
//NewEmitter class ochib EventEmitterdan meros olish
var NewEmitter = /** @class */ (function (_super) {
    __extends(NewEmitter, _super);
    function NewEmitter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewEmitter;
}(events_1.EventEmitter));
//NewEmitter dan instance(andoza) olish
var newEmitter = new NewEmitter();
//event listener
newEmitter.on(events_enum_1.EventsEnum.HELLO, function () {
    console.log('Hello World');
});
newEmitter.on(events_enum_1.EventsEnum.BYE, function () {
    console.log('Bye World');
});
//qabul qilib olish
newEmitter.emit(events_enum_1.EventsEnum.HELLO);
newEmitter.emit(events_enum_1.EventsEnum.BYE);
