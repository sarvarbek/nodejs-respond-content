"use strict";
exports.__esModule = true;
exports.EventsEnum = void 0;
var EventsEnum = /** @class */ (function () {
    function EventsEnum() {
    }
    EventsEnum.HELLO = 'hello';
    EventsEnum.BYE = 'bye';
    return EventsEnum;
}());
exports.EventsEnum = EventsEnum;
